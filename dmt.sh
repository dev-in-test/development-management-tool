#!/bin/bash

# V ----------------------- Variables -------------------
RED='\033[0;41;30m'
STD='\033[0;0;39m'

REPO="https://darboo@bitbucket.org/darboo/development-management-tool.git"

clear

SAI="sudo apt install -y "

# Allows exiting of sub menus
quitsub(){
	clear && quit=Y
}
     
 

# 5 ------------------------------ Server Menu -----------------------------

servermenu(){

server_menus(){

MenuTitle=" Server Options "
MenuDescription=" Perform maintainance and other usefull server commands "

	echo " "	
	echo " $MenuTitle " 
	echo " $MenuDescription " 
	echo " -------------------------------- "
	echo "10.  Update, Upgrade and Autoremove "
	echo "20.  Midnight Commander "
	echo "21.  TaskSel "
	echo "30.  "
	echo "40.  "
	echo "50.  "
	echo "70.   "
    	echo "80.  Reboot Server"
	echo "81.  Shutdown Server"
	echo "90.   "
	echo "99. Return to Main Menu "
	echo
	echo $report
}

server_options(){

local choice
	read -p "Enter choice [ 1 - 99] " choice
	case $choice in
		10) updupg && autorem ;;
		20) mc ;;
		21) runtasksel ;;
		30)  ;;
		40)  ;;
		50)  ;;
		60)  ;;
		70)  ;;
		80) sudo reboot ;;
		81) sudo shutdown now ;;
		99) quit="Y" ;;
		*) echo -e "${RED} You can't do that..... ${STD}" && sleep 2
	esac

}
    while [ "$quit" != "Y" ] 
    do
	server_menus
	server_options
    done
}

# 4 ************************ Snap menu *******************************

snapinsta(){
	read -rp "Enter the Snap to install: " instsnap
	snap install $instsnap
}
snapquery(){
	read -rp "Search the SnapStore for: " findsnap
	snap find $findsnap
}
snapupd(){
	read -rp "Enter the Snap to update or leave empty for all: " updsnap
	snap refresh $updsnap
}
snaprem(){
	read -rp "Enter the Snap to remove: " remsnap
	snap remove $remsnap
}
snapdet(){
	read -rp "Display Details about which Snap? " detsnap
	snap info $detsnap
}
snapmen() {
snap_menu() {

	echo "~~~~~~~~~~~~~~~~~~~~~"	
	echo " SNAP - MENU"
	echo "~~~~~~~~~~~~~~~~~~~~~"
	echo "1. install a Snap Package"
	echo "2. Search for a Snap"
	echo "3. Update one or all Snaps"
	echo "4. Remove a Snap"
	echo "5. list Snaps "
	echo "6. Snap Details"
	echo "7.  "
	echo "99. Return to Main Menu "
}

snap_options(){
	local choice
	read -p "Enter choice [ 1 - 99] " choice
	case $choice in
		1) snapinsta ;;
		2) snapquery ;;
		3) snapupd ;;
		4) clear && snap list && snaprem && snap list;;
		5) clear && snap list ;;
		6) clear && snap list && snapdet ;;
		9)  ;;
		99) quitsub ;;
		*) echo -e "${RED} $choice is not available today.${STD}" && sleep 2
	esac
}

while [ "$quit" != "Y" ]
do
	snap_menu
	snap_options
done
}

# 3 -------------------- git menu -------------------------


file2git(){
	read -rp "Enter the name of the file or,' . 'for all files: " gitfile 
}
all2git(){
	gitfile="."
}
gitpull(){
	git pull
}
gitadd(){
	git add $gitfile
}
gitcommit(){
	git commit 
}
gitpush(){
	git push 
}
ggyes(){
# this switch makes these details globall (not implemented)
	gitglo="--global "
}
gitUserConfig(){
	read -rp "Enter your email: " gitmail
	git config $gitglo user.email "$gitmail"
	read -rp "Enter your username: " gituser
	git config $gitglo user.name "$gituser"
}
gitmenu(){
git_menu() {

	echo "~~~~~~~~~~~~~~~~~~~~~"	
	echo " GIT - MENU"
	echo "~~~~~~~~~~~~~~~~~~~~~"
	echo "1. Update DMT "
	echo "2. Add files to repository. "
	echo "3. Commit "
	echo "4. Push "
	echo "5. Add All Files & Commit "
	echo "6. Add All Files, Commit & Push "
	echo "7. GIT User Configuration "
	echo "99. Return to Main Menu " 
    echo "------------------------"
    echo " $lastmessage " 
    echo " "
	echo "------------------------"

}

git_options(){
	local choice
	read -p "Enter choice [ 1 - 99] " choice
	case $choice in
		1) gitpull ;;
		2) file2git && gitadd ;;
		3) gitcommit ;;
		4) gitpush ;;
		5) all2git && gitadd && gitcommit ;;
		6) all2git && gitadd && gitcommit && gitpush ;;
		7) clear && gitUserConfig ;;
		99) quitsub ;;
		*) echo -e "${RED} where did you see? $choice ${STD}" && sleep 2
	esac
}

while [ "$quit" != "Y" ]
do
	git_menu
	git_options
done
}

# 2 -------------------- LXD Menu ---------------------

# 2a *************** install LXD and its requirements *******************************
LXDsetup(){
# Update, upgrade and autoremove
	sudo apt update
        sudo apt upgrade -y
	sudo apt autoremove -y
# Install lxd:
	$SAI lxd

# Install ZFS
	$SAI zfsutils-linux
# install additional LXC tools
	$SAI lxc-utils

# Install Midnight Commander
	$SAI mc

# Set git push default to stop that message
  git config --global push.default simple

# Initialise LXD (must make a config file for this......) 
	echo "**"
	echo "Starting the interactive configuration,"
        echo "please select the default options:"
	echo "**"
	sudo lxd init 
# Add the current user to the LXD Group:
	sudo usermod --append --groups lxd $USER

# notification message to display on the menu screen
	lastmessage="---LXD/LXC, and Midnight commander installed, 
	please RESTART YOUR MACHINE before you create containers.---"
}

# 2 ********************* Creating Containers *******************

# Ephemeral container
Ephemcont(){
	read -rp "The name of this Ephemeral Container is: " newcon
	lxc launch -e ubuntu:18.04 $newcon
	waiting
	updupgre
	lastmessage=" $newcon will be destroyed if shutdown "
}

#New Container name
contname(){
	read -rp "Enter the name of the target Container: " newcon
}
#Create an Ubuntu18.04 container
makecon(){
	contname
	lxc launch ubuntu:18.04 $newcon
	waiting
	updupgre
	insmc
	lastmessage="The $newcon container is now ready for use"
	findip4
}
waiting(){
	echo $newcon "awaits an ip address, LXD will spit one out eventualy......"	
	sleep 6
}
#Update, upgrade and autoremove
updupgre(){
	lxc exec $newcon -- sudo apt update
        lxc exec $newcon -- sudo apt upgrade -y
	lxc exec $newcon -- sudo apt autoremove -y
}
dpkgfix(){
	sudo dpkg --configure -a
}
# Create an nginx container 
makenginx(){
	contname
	lxc launch ubuntu:18.04 $newcon
	waiting
	updupgre
	insmc
	lxc exec $newcon -- sudo apt install nginx -y
	findip4
	lastmessage="The NGINX container $newcon is now ready for use"
}

# 2 ******************** Container Commands ******************
pause(){
  read -p "Press [Enter] key to continue..." fackEnterKey
}

#List available containers
status(){
	lxc list --format csv -c ns4tpaS	
}
#Display the contents of the Containers log file
conlog(){
	lxc info $newcon --show-log 
}

# Start Container(s)
start(){
	lxc start $newcon
}
# Stop Container(s)
stop(){
	lxc stop $newcon
}
# Restart Container(s)
resta(){
	lxc restart $newcon
}
# Login as the user Ubuntu
logubu(){
	contname
	lxc exec -v $newcon -- sudo --login --user ubuntu
}
# Delete a Container
delcon(){
	lxc stop -v $newcon 
	echo "Destroying the $newcon Container......"
	sleep 4
	lxc delete $newcon -v
	lastmessage="the Container $newcon is no longer with us... "
}


# 3 ************ Install Authorized Software *************
#Install Midnight Commander
insmc(){
	lxc exec $newcon -- $SAI mc
}


# 5 ***********************************************

contdetails(){
	contname
	lxc config show --expanded $newcon
}

shtdwn(){
	lxd shutdown
	echo " All Containers Stopped "
}

# Login to a Container
loginroot(){
	status
	read -rp "Enter a name for the target Container: " newcon
	lxc exec $newcon -- /bin/bash
}

# 6 ----------------waiting for an ip -------------------------------
wait4ip(){
	
findip4(){
	contip=[ lxc list --format csv -c 4 ]
}
gotip4(){
	if [ "$contip" = " " ]; then
	echo "waiting....." $contip
else
	echo " $contip " && quit=Y
	fi	
}
while [ "$quit" = "N" ]
do
	findip4
	gotip4
done

}

# 7 --------------------start and stop containers----------------------
stasto(){

startstop_menu(){
#	clear
	status
	echo "~~~~~~~~~~~~~~~~~~~~~"	
	echo " Start or stop one or all containers "
	echo "~~~~~~~~~~~~~~~~~~~~~"
	echo ""
	echo "1. Start a Container."
	echo "3. Start all Containers."
	echo "5. Stop a Container."
	echo "6. Stop all Containers."
	echo "7. Restart a Container."
	echo "8. Restart all Containers."
	echo "9. Delete a Container."
	echo "90.Return to Main Menu"
	echo "99. exit programme"
	echo " "
	echo " $lastmessage " 
	echo " $newcon "
}

startstop_options(){
	local startstop_choice
	read -p "Enter choice [ 1 - 99] " startstop_choice
	case $startstop_choice in
		1) contname && start ;;
		3)  ;;
		5) contname && stop ;;
		6)  ;;
		7) contname && resta ;;
		9) contname && delcon ;;
		90) quit=Y ;;
		99) exit ;;
	*) echo -e "${RED}I have no idea what $startstop_choice means.....${STD}" && sleep 3
	esac
}

# Loop while quit is N
while [ "$quit" = "N" ]
do
	startstop_menu
	startstop_options
done
}
# 8 LXD - menu ***********************************************************
LXDmenu() {
	MenuTitle=" LXD - Options"
	Description=" Tools to manage Containers "
LXD_menu() {

	echo " "	
	echo " $MenuTitle "
	echo " $Description "
	echo "-----------------------------------"
	echo "1. "
	echo "2. List all containers. "
        echo "3. Create a basic Container and Login as ubuntu. "
	echo "4. Create an NGINX container."
	echo "5. Login to a Container."
	echo "6. Start/Stop Containers. "
        echo "7. Create an Ephemeral container. "
        echo "8. Detailed container information. "
	echo "9. Delete a Container. "
	echo "99. Return to Main Menu "
}

LXD_options(){
	local choice
	read -p "Enter choice [ 1 - 99] " choice
	case $choice in
		1)  ;;
		2) clear && status ;;
		3) contname && makecon && wait4ip && updupgre && insmc && logubu ;;
		4) makenginx ;;
		5) logubu ;;
		6) stasto ;;
                7) Ephemcont ;;
                8) clear && contdetails ;;
		9) contname && delcon ;;
		10) ;;
		99) quitsub ;;
		*) echo -e "${RED}Error...${STD}" && sleep 3
	esac
}

while [ "$quit" = "N" ]
do
	LXD_menu
	LXD_options
done
}


# 1 -------------------- Admin Menu --------------------


setupansible(){
    SWupdate
    SWupgrade
    $SAI software-properties-common
    sudo apt-add-repository ppa:ansible/ansible -y   
    SWupdate
    $SAI ansible
    SWautoremove
    cp -R /etc/ansible /home/$USER/ansible
    cd /home/$USER/ansible
    lastmessage="- Ansible installed -"
}

# Update, upgrade and autoremove
SWupdate(){
	sudo apt update
	lastmessage=" OS Updated "
}

SWupgrade(){
    sudo apt upgrade -y
    lastmessage=" OS Upgraded " 
}

SWautoremove(){
    sudo apt autoremove -y
    lastmessage=" Removed unecassary software "
}

# fix Broken Packages
dpkgfix(){
	sudo dpkg --configure -a
	lastmessage=" Package Manager Repaired "
}

#Install Midnight Commander
insmc(){
	$SAI mc 
	lastmessage=" Midnight Commander Installed " 
}

# Install ZFS
InstallZFS(){
	$SAI zfsutils-linux
	lastmessage=" ZFS ready "
}
# add a PPA 
addppa(){
    read -p " apt-add-repository ppa: " PPA
    sudo apt-add-repository ppa: $PPA
    lastmessage=" Added the $PPA ppa "
}
# SSH Setup
setupssh(){
	read -p " Provide USER@SERVER details for target SSH machine" sshtarget
	ssh keygen
	ssh-copy-id $sshtarget
}
# SSH Login
loginssh(){
	read -p " Provide USER@SERVER details for target SSH machine" namessh
	ssh $namessh
	lastmessage=" Logged into $namessh "
}

BootFix(){
	sudo add-apt-repository ppa:yannubuntu/boot-repair -y
	SWupdate
	$SAI boot-repair
	boot-repair
}

instasksel(){
	$SAI tasksel tasksel-data
}
runtasksel(){
	sudo tasksel
}
adminmenu(){

MenuTitle=" Admin Menu "
Description=" System administration and management tools "

        admin_menu(){
	echo " "	
	echo " $MenuTitle "
	echo " $Description "
	echo "-----------------------------------"
	echo ""
	echo "1.  Install ZFS"
	echo "2.  Install Ansible "
	echo "3.  Install Midnight Commander "
	echo "4.  Update the Host machine "
	echo "5.  Upgrade the Host machine "
	echo "6.  Cleanup (autoremove) "
    	echo "7.  Update, Upgrade & Autoremove "
	echo "8.  Add a PPA source "
	echo "9.  Fix Broken Packages "
	echo "10. SSH Setup"
	echo "11. SSH Login"
	echo "12. Boot Repair "
	echo "20. Install Tasksel "
	echo "21. install LXD"
	echo "99. Return to Main Menu " 
    echo "------------------------"
    echo " $lastmessage"
	echo "------------------------"

}
     admin_options(){
	local choice
	read -p "Enter choice [ 1 - 99] " choice
	case $choice in
		1) InstallZFS ;;
		2) setupansible ;;
		3) insmc ;;
		4) SWupdate ;;
		5) SWuppgrade ;;
		6) SWautoremove ;;
       		7) SWupdate && SWupgrade && SWautoremove ;;
		8) addppa ;;
		9) dpkgfix ;;
		10) setupssh ;;
		11) loginssh ;;
		12) BootFix ;;
		20) instasksel ;;
		21) LXDsetup ;;
		99) clear && quitsub ;;
		*) echo -e "${RED} $choice is not a displayed option.....${STD}" && sleep 2
	esac
}

while [ "$quit" != "Y" ]
do
	admin_menu
	admin_options
done
}   
    
# 0 ----------------------- Main Menu -----------------------

MenuTitle=" - Development Management Tools - "
Description=" A simple BASH menu for software development, deployment and managment. "

# Display menu options
show_menus() {

	echo " "	
	echo " $MenuTitle "
	echo " $Description "
	echo "-----------------------------------"
	echo ""
	echo "1.  Admin Tools."
	echo "2.  Virtualisation "
	echo "3.  GIT options. "
	echo "4.  Snap Options. "
	echo "5.  Server Tools. "
	echo "0.  Readme " 
	echo "10. Exit "
	echo " "
	echo " $lastmessage " 
	echo "  " 
	
}

read_options(){
	local choice
	read -p "Enter the desired item number: " choice
	case $choice in
		1)  clear && adminmenu ;;
		2) clear && LXDmenu ;;
		3) clear && gitmenu ;;
		4) clear && snapmen ;;
		5) clear && servermenu ;;
		0) nano ./README.md ;;
        10) clear && echo " See Ya! " && exit 0;;
		*) echo -e "${RED} $choice is not a displayed option.....${STD}" && sleep 3
	esac
}
 

# - Trap CTRL+C, CTRL+Z and quit singles disabled while developing -
#trap '' SIGINT SIGQUIT SIGTSTP

# --------------- Main loop --------------------------
while true
do
#the quit variable allows menus to exit when its value is changed to = Y,
#this must be reset to = N upon breaking the loop.	 
	quit="N"
	show_menus
	read_options
done

