This is a work in progress, some features have yet to be implemented, have been removed or broken.  
  
The Development Management Tool acts as an interface for some commonly used development and testing tools for a mobile device via ssh.  
Written in bash, it can performs tasks from a list of options selected with numbers, this helps to avoid performing repetative manual operations.  
  
System Administration - install and configure a host machine with the desired software and device maintenace tools.  
Assist in the development of automating manual systems to create workflows with CI and testing.  
Create development and testing environments with Virtual Machines, Containers or a combination of both.  
Manage version control with GIT.  
Command knowledge not required since it utilises an interactive BASH menu.   
An aid to grasp the concepts of Virtualisation and its place in the cloud environment.  
Understand and leverage Ansible, Virtual Machines and Containers.  


This scripts is being developed and tested with:   
ubuntu 18.04 desktop/server on 64Bit AMLogic-S905X, AMD & Intel devices.  

Software Requirements:  
GIT

Software DMT can install:  
Tasksel  
Ansible  
zfsutils-linux  
Midnight Commander  


#Getting Started  
To get going, clone this repo to a location of your choice, and run ./dmt.sh .  

You will now be presented with:

The Main Menu  

  Server Options  
  A simple BASH menu for software development, deployment and managment.  
-----------------------------------

1.  Admin Tools.
3.  GIT options. 
4.  Snap Options. 
5.  Server Tools. 
0.  Readme 
10. Exit 
 

The options listed above contain the sub-menus listed below.  
Incomplete menu items indicate missing features, but what is here (except ansible) should work.  


Option 1 - Admin Menu  
~~~~~~~~~~~~~~~~~~~~~  
 ADMIN - MENU  
~~~~~~~~~~~~~~~~~~~~~  
1. Install ZFS  
2. Install Ansible 
3. Install Midnight Commander 
4. Update the Host machine 
5. Upgrade the Host machine 
6. Cleanup (autoremove) 
7. Update, Upgrade & Autoremove 
8. Add a PPA source 
9. Fix Broken Packages 
10. SSH Setup
11. SSH Login
12. 
99. Return to Main Menu 
  

Option 3 - GIT  
These options relate to the git enabled directory you launched DMT from.  

~~~~~~~~~~~~~~~~~~~~~
 GIT - MENU
~~~~~~~~~~~~~~~~~~~~~
1. Pull
2. Add files to repository.  
3. Commit  
4. Push  
5. Add All Files & Commit  
6. Add All Files, Commit & Push  
7. Repository Options   
99. Return to Main Menu  
------------------------
  

Option 3-7 - Repository Options  

~~~~~~~~~~~~~~~~~~~~~  
  Repository- MENU   
~~~~~~~~~~~~~~~~~~~~~~   
1.  
2. Change current GIT/bitbucket Repository  
3.  
4.  
5.  
6.  
7.  Return to GIT Menu
99. Return to Main Menu  
  
Option 4 - Snap  
~~~~~~~~~~~~~~~~~~~~~  
 SNAP - MENU   
~~~~~~~~~~~~~~~~~~~~~   
1. install a Snap Package  
2. Search for a Snap  
3. Update one or all Snaps  
4. Remove a Snap  
5. list Snaps  
6. Snap Details  
7.   
99. Return to Main Menu 

Option 5 – Server Tools
  Server Options  
  Perform maintainance and other usefull server commands  
 -------------------------------- 
10.  Update, Upgrade and Autoremove 
20.  Midnight Commander 
21.  TaskSel 
30.  
40.  
50.  
70.   
80.  Reboot Server
81.  Shutdown Server
90.   
99. Return to Main Menu 
 

#Known Issues
DMT should be in an existing local path    
Ansible install is incomplete  
Virtual Machines & Containers still ongoing. 

